import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable()
export class RestApiService {
  constructor(private http: HttpClient) { }
  
  allNodes = '/node/all';

  getAllNodes(): Observable<any> {
    
    return this.http.get(this.allNodes,{ headers: { 'Content-Type': 'application/json' } });
  }
}