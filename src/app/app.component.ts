import { Component } from '@angular/core';
import { RestApiService } from './Services/rest.api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Discovery planetary travels';

  nodes: Node[]=[];
  entries: string []=[]
  refNode:string;
  selectedEntry;

  constructor(
    private restApi: RestApiService
    ) {}

  ngOnInit(){

    this.restApi.getAllNodes().subscribe( data =>{

      this.nodes=data;
      this.refNode=(this.nodes[0]).nodeName;
      

    });

  }
    
  onSelectionChange(entry) {
    this.selectedEntry = Object.assign({}, this.selectedEntry, entry);
  }
}
